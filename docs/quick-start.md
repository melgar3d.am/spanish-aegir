# Guía de inicio rápido

## Para servidores con Fully Qualified Domain Name (<a href="https://www.ionos.es/digitalguide/dominios/gestion-de-dominios/fully-qualified-domain-name/" target="_blank">FQDN</a>)

Sobre las últimas versiones de servidores Debian o Ubuntu con Fully Qualified Domain Name (FQDN) ejecuta los siguientes comandos:

```sh
# Debian 10 (Buster), Ubuntu 18.04 and later:
sudo apt install mysql-server php-mbstring nginx php-cli php-fpm php-gd php-pear php-zip php-curl
sudo mysql_secure_installation
sudo wget -O /usr/share/keyrings/aegir-archive-keyring.gpg https://debian.aegirproject.org/aegir-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/aegir-archive-keyring.gpg] https://debian.aegirproject.org stable main" | sudo tee -a /etc/apt/sources.list.d/aegir-stable.list
sudo apt update
sudo apt install aegir3 aegir-archive-keyring
```
## Para servidores sin Fully Qualified Domain Name (<a href="https://www.ionos.es/digitalguide/dominios/gestion-de-dominios/fully-qualified-domain-name/" target="_blank">FQDN</a>)

Para crer una instancia de Aegir para desarrollo o pruebas en una máquina virtual remota o local sin FQON, siga las siguientes instrucciones  <a href="https://gitlab.com/aegir/aegir-dev-vm" target="_blank">Aegir Development VM</a>.

### Lectura recomendada

Para asegurarse de seguir unas prácticas seguras en la instalación, se recomienda leer la siguiente información [Guía de instalación](/install). Contiene información importante sobre la guía de instalación en otros sistemas operativos.