Aegir Hosting System
====================

Aegir es un potente sistema de hospedaje web en sistemas LAMP o LEMP para el despliegue y administración de webs desarrolladas en Drupal.

* Notas de la última versión: [3.18](/release-notes/3.18/)

con Aegir, puedes configurar un sitio Drupal con unos pocos clicks. Aegir se encarga de crear los ficheros de configuración del servidor web del sitio, la base de datos, arranca los procesos de instalación de Drupal y recarga los servicios necesarios, todo automáticamente.

Esta documentación te ayudará a entender Aegir y como [se instala](/install/), así como a [usarlo](/usage/). A demás, para usuarios avanzados, puedes encontrar una [lista de extensiones](extend/contrib.md), así como otros mecanismos que te permitirán [extender y personalizar su comportamiento](extend.md). Finalmente, para quien esté interesado en personalizar Aegir o contribuir a corregir errores o nuevas características, hay una sección para [desarrollar en Aegir](develop.md).

Aegir es software libre, y por ello existe una [comunidad](community.md) dispuesta a ayudar.

Si necesitas apoyo profesional para la instalación, visita la [Página de proveedores de servicios](/community/services/).
